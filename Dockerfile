FROM node:10-alpine

WORKDIR /Users/Projects/Node_hello

COPY app.js .

RUN npm init -y && npm install express --save

CMD node app.js